[![License](http://img.shields.io/:license-apache2-green.svg?style=flat-square)](https://www.apache.org/licenses/LICENSE-2.0)
# OWASP Top 10  - 2020 Edition

Python code and hints to OWASP top 10 hacking challenges (Edition 2020) 

### Usages code code

Install dependencies with ```python3 -m pip install -r requirements.txt```

### Hints

Following are only hints to the challenges, not the solutions. You will have to develop the exploits yourself. 

The application server was written in Java und is deployed in a TomCat standalone server. 

##### 6111 - OWASP 2010 - A1 - Injection

Problem: No prepared statement used in backend to access MySQL database. 

Attack: Used Ambigous expression in input field. Since we want to access a certain user's account, our task will be injecting SQL code into the password field.

Mitigation: Use prepared statement.

![SQL-Injection](https://github.com/CodeAndChoke/owasp-top-ten-hints/blob/master/images/001-sql-injection.png)

##### 6112 - OWASP 2010 - A2 - Cross-Site Scripting

Problem: No proper HTML escaping was implemented. User's input was displayed as it is and will be executed by Browser.

Attack: Set up a HTTP server which can be used to obtain victim's cookies. The commenting site of the web app is exploitable, write a small JS script that send the session's cookie to the HTTP server and send it to the commenting page. When another user goes to this page, the script will be executed in his session.

Mitigation: Use proper HTML escaping in backend or escape user's input in Frontend.

##### 6113 - OWASP 2010 - A3 - Broken Authentication and Session Management

Problem: A hacker can obtain a cookie and send a link with the cookie to victim like this: https://glocken.hacking-lab.com/12001/url_case3/url3/controller?action=profile&AValue=COOKIEabcdefgh and if the victim clicks on the link and authenticates himself, the hacker can use this cookie to access the victim's data.

Attack: Attacker obtains a cookie, send a link with a cookie to user. User authenticated himself, the sent cookie therefore will be authenticated as victim as well.

Mitigation: Usage of two cookies. Cookie only works for authenticated IP, etc.

##### 6114 - OWASP 2010 - A4 - Insecure Direct Object References

Problem: Server only checks for authentication. No authorization checking was implemented.

Attack: Hacker logins with his own password and can see other user's data.

Mitigation: Implement an authorization concept

##### 6115 - OWASP 2010 - A5 - Cross Site Request Forgery

Problem: HTTP is a stateless protocoll. Server only cares if the requests are valid and authenticated and doesn't care for the context.

Attack: Send a malicious website to an `authenticated` victim with code that makes requests to server to add items to the shopping cart and execute a payment. The victim will pay money for the items at the moment he clicks on the link and the requests are made. For this purpose it is advised to create a simple HTTP server that serves the malicious code. 

Mitigation: Only executes requests from certain origins.

##### 6116 - OWASP 2010 - A6 - Security Misconfiguration

Problem: Server uses XML query to query data. But the query was made in client and is not verified.

Attack: Use malicious query to read file's system. This can be done properly with an external HTTP client like cURL or Python requets.

Mitigation: Build query on server. Configure server's file system permissions. Validate user's input.

##### 6117 - OWASP 2010 - A7  Insecure Cryptographic Storage

Problem: Server doesn't use prepared statements. Passwords aren't hashed.

Attack: SQL injection with sqlmap.

```
sqlmap -u http://glocken.hacking-lab.com/12001/inputval_case3/inputval3/controller\?words\=test\&send\=suchen\&action\=search --tables --hex --threads 4

sqlmap -u http://glocken.hacking-lab.com/12001/inputval_case3/inputval3/controller\?words\=test\&send\=suchen\&action\=search -D glocken_emil -T customers --dump

sqlmap -u http://glocken.hacking-lab.com/12001/inputval_case3/inputval3/controller\?words\=test\&send\=suchen\&action\=search -D elgg -T elgg_users --dump
```

Mitigation: Use prepared statement. Hash properly bith BCrypt, use OAuth.

##### 6118 - OWASP 2010 - A8 - Failure to Restrict URL Access

Problem: Server doesn't show admin urls publicly. But the URLs are not restricted either.

Attack: Find the possible paths with exploit of challenge 6. Server was written in Java so it is pretty simple. Trial and error until the correct path is found. Go to the TomCat directory, find the server's version of the challenge and find the JSP file that is responsible for the target page. 

Mitigation: Role concepts for URLs. Since the server was written in Java, use Filter.

##### 6119 - OWASP 2010 - A9 - Insufficient Transport Layer Protection
Problem: TLSs are not used everywhere. Payloads from clients are secured. But transmission of many other files are not secured, which btw. also contains the cookie. 

Attack: Passive attack with Wireshark.

Mitigation: Consistent usage of TLS.

##### 6120 - OWASP 2010 - A10 - Unvalidated Redirects and Forwards

Problem: Server redirects URL without checking if user has access to the URL.

Attack: Login with own account but redirect to other user's account. This attack can be done properly with a HTTP client like cURL or Python requests.

Mitigation: User input's validation. Implement an authorization concept.



